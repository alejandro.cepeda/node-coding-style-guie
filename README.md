# Ideas para escribir buen codigo en Node

Cuando rescribes codigo, lo haces tambien para que eventualmente otros lo lean. Es importante establecer un standard en tu grupo, esto evitara perder tiempo en entender lo que la otra persona quiso hacer.

# Tabla de contenido

* Usar UPPERCASE para nombrar constantes
* Usar comillas simples en cadenas de solo string
* Usar templates literales en lugar de concatenar strings
* LLaves de apertura en la misma linea
* Usar lowerCamelCase para nombrar variables, funciones, propiedades, metodos, clases
* Usar el operador de triple igualdad ===
* Regresar lo antes posible el valor de las funciones
* Usar metodos encadenados por linea
* Requires y otras dependencias en la parte superior
* Usar nombres descriptivos en variables, funciones, propiedades, metodos, clases
* Usar let y const


